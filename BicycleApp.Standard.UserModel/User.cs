﻿using System;

namespace BicycleApp.Standard.UserModel
{
    public class User
    {
        #region properties
        public string UserName { get; set; }
        public string UserLocation { get;  set; }
        public string UserNumber { get;  set; }
        public bool IsUserBlocked { get; set; } = false;

        public override string ToString()
        {
            return $"UserName:::::{UserName}\tUserLocation::::{UserLocation}\tUserNumber::::{UserNumber}";
        }
        #endregion
    }
}

