﻿using BicycleApp.Standard.Models;
using BicycleApp.Standard.Repository.Constant;
using BicycleApp.Standard.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BicycleApp.Standard.Repository
{
    public class BicycleRepository : IBillable
    {
        #region basic sql connection
        /*SqlConnection sqlConnection = null;
        SqlCommand Cmd = null;
        public BicycleRepository()
        {
            sqlConnection = new SqlConnection("server=DESKTOP-EC2DQJL;DataBase=BicycleDb;Integrated Security=true");
            Cmd = new SqlCommand();

        }
        public int AddBicycle()
        {
            try {
                sqlConnection.Open();
                Cmd.CommandText = "insert into tbl_Bicycles Values(1,'Miracle',700,1,15)";

                Cmd.Connection = sqlConnection;
                int addBicycleResult = Cmd.ExecuteNonQuery();
                sqlConnection.Close();
                return addBicycleResult;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);



            }

            public List<Bicycle> GetAllBicycles()
            {

                List<Bicycle> bicycles = new List<Bicycle>();

                Cmd.CommandText = "select * from tbl_Bicycles";
                Cmd.Connection = sqlConnection;
                sqlConnection.Open();
                SqlDataReader sqlDataReader = Cmd.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Bicycle bicycle = new Bicycle();
                    bicycle.Id = Convert.ToInt32(sqlDataReader["Bicycleid"]);
                    bicycle.Name = sqlDataReader["Bicyclename"].ToString();

                    bicycle.Price = Convert.ToInt32(sqlDataReader["Bicycleprice"]);

                    bicycle.Quantity = Convert.ToInt32(sqlDataReader["Bicyclequantity"]);

                    bicycle.Distance = Convert.ToInt32(sqlDataReader["Bicycledistance"]);

                    bicycles.Add(bicycle);

                }

                sqlConnection.Close();




                return bicycles;
            }
           


        }*/
        #endregion
        #region sqladdapter connection
        SqlDataAdapter sqlDataAdapter = null;
        DataSet dataset;
        public BicycleRepository()
        {
            sqlDataAdapter = new SqlDataAdapter("select * from tbl_Bicycles", "server = DESKTOP - EC2DQJL; DataBase = BicycleDb; Integrated Security = true");
            dataset = new DataSet();
            sqlDataAdapter.Fill(dataset, "tbl_Bicycles");


        }

        public void AddBicycle(Bicycle bicycle)
        {
            DataRow dataRow = dataset.Tables["tbl_Bicycles"].NewRow();
            dataRow["Id"] = bicycle.Id;
            dataRow["Name"] = bicycle.Name;
            dataRow["Price"] = bicycle.Price;
            dataRow["Quantity"] = bicycle.Quantity;
            dataRow["Distance"] = bicycle.Distance;
            dataset.Tables["tbl_Bicycles"].Rows.Add(dataRow);

        }

        public List<Bicycle> GetAllBicycles()
        {

            List<Bicycle> bicycles = new List<Bicycle>();

            foreach (DataRow row in dataset.Tables["tbl_Bicycles"].Rows)
            {
                Bicycle bicycle = new Bicycle();
                bicycle.Id = Convert.ToInt32(row["Id"]);
                bicycle.Name = row["Name"].ToString();

                bicycle.Price = Convert.ToInt32(row["Price"]);

                bicycle.Quantity = Convert.ToInt32(row["Quantity"]);

                bicycle.Distance = Convert.ToInt32(row["Distance"]);

                bicycles.Add(bicycle);

            }
            return bicycles;
        }

        public void UpdateChanges()
        {
            SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
            sqlDataAdapter.Update(dataset, "tbl_Bicycles");
        }
        #endregion


        public void GenerateInvoice(User user, InvoiceOptions invoiceOptions)
        {
            List<Bicycle> bicycles = new List<Bicycle>();
            foreach (Bicycle bicycle in bicycles)
            {
                int totalAmount = 0;
                totalAmount += bicycle.Price * bicycle.Quantity;
            }

        }
       



      
       
    }
}
