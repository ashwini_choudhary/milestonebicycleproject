﻿using BicycleApp.Standard.Repository.Constant;
using BicycleApp.Standard.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BicycleApp.Standard.Repository
{
    internal interface IBillable
    {
        public void GenerateInvoice(User user, InvoiceOptions invoiceOptions);
    }
}
