﻿using BicycleApp.Standard.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BicycleApp.Standard.Repository
{
    public class UserRepository:IUserRepository
    {
        List<User> _users;
        public UserRepository()


        {

            _users = new List<User>();

        }

        public bool AddUser(User user)
        {
            _users.Add(user);
            return true;
        }
        public List<User> GetAllUsers()
        {


            return _users.FindAll(v => v.IsUserBlocked == false);

        }
        public User GetUserByName(string userNameToSearch)
        {
            return _users.Find(u => u.UserName == userNameToSearch);
        }



    


    }
}
