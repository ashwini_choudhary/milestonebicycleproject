﻿using BicycleApp.Standard.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BicycleApp.Standard.Repository
{
    public interface IUserRepository
    {
        bool AddUser(User user);
        List<User> GetAllUsers();
        User GetUserByName(string userNameToSearch);

    }
}