﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BicycleApp.Standard.Repository.Constant
{
    public  enum InvoiceOptions
    {
        Email=1,
        Pdf,
        Print,
        OnScreen,
        WhatsApp
    }
}
