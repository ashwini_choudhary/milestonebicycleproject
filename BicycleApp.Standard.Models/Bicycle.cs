﻿using System;

namespace BicycleApp.Standard.Models
{
    public class Bicycle
    {
        #region properties
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }

        public int Quantity { get; set; }


        public int Distance { get; set; }

        public override string ToString()
        {
            return $"BycycleId::::::{Id}\tName:::{Name}\tBicyclePrice ::::{Price}\tBicycleQuantity ::::{Quantity}\tBicycleDistance::::{Distance}";
        }
        #endregion
    }
}
